import java.text.DecimalFormat;

public class Rechner 
{
  public static void main(String[] args)
  {
	  double val1 = 2.5; // Double : Werte, die entweder ein Bruch sind oder mit einem Bruch enden
    
      DecimalFormat df = new DecimalFormat("#.00"); //können Gleitkommazahlen als Text darstellen, die Art der Darstellung kann dabei in verschiedener Weise gesteuert werden.
      System.out.println("Ergebnis: "+df.format(val1));
  }
}